{ config, pkgs, ... }:
{
  nixpkgs.config.allowBroken = true;
  nixpkgs.config.allowUnfree = true;
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.enableUnstable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelModules = [ "kvm-intel" ];
  boot.kernelParams = [ "mem_sleep_default=deep" "nohibernate" ];
  boot.initrd.supportedFilesystems = ["zfs"];
  boot.initrd.postDeviceCommands = "zfs rollback -r rpool/local/root@blank";

  networking.hostName = "theseus"; # Define your hostname.
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";
  time.hardwareClockInLocalTime = true;

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.hostId = "e7d11eb2";

  virtualisation.libvirtd.enable = true;
  virtualisation.podman.enable = true;
  
  environment.systemPackages = with pkgs; [
    #factorio
    obs-studio
    mpv
    ffmpeg
    bash
    blueberry
    bluez
    boundary
    consul
    curl
    direnv
    discord
    exodus
    file
    firefox
    fprintd
    fwupd
    gammastep
    git
    htop
    jq
    kanshi
    kitty
    libfprint
    lorri
    mako
    neovim
    nomad
    pass
    pavucontrol
    podman
    ripgrep
    slack
    spotify
    sway
    usbutils
    vagrant
    vault
    vim 
    vscode
    waypoint
    wdisplays
    wget
    zoom
    zsh
    zfs
  ];

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Update firmware 
  services.fwupd.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  
  # Configure keymap in X11
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "ctrl:swapcaps";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Swap Caps Lock to CTRL/ESC
  services.interception-tools.enable = true;

  # Enable sound.
  #sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };


  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.nikolai = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "input" "libvirtd" ];
  };

  security.sudo.wheelNeedsPassword = false;
  security.pam.services.login.fprintAuth = true;

  # Power Management
  powerManagement.powertop.enable = true;
  powerManagement.cpuFreqGovernor = "powersave";
  
  # Programs
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  
  programs.sway = {
    enable = false;
    wrapperFeatures.gtk = true;
    extraPackages = with pkgs; [
      gammastep
      slurp
      grim
      light
      mako
      wofi
      swayidle
      swaylock
      swaybg
      waybar
      wdisplays
      wl-clipboard
      wshowkeys
      xwayland
    ];
  };
  
#  systemd.user.services.kanshi = {
#    description = "Kanshi output autoconfig ";
#    documentation = [ "man: kanshi(1)" ];
#    wantedBy = [ "graphical-session.target" ];
#    partOf = [ "graphical-session.target" ];
#    serviceConfig = {
#      ExecStart = "${pkgs.kanshi}/bin/kanshi";
#      RestartSec = 5;
#      Restart = "always";
#    };
#  };
#
#  systemd.user.services.mako = {
#    description = "Mako notification daemon";
#    documentation = [ "man: mako(1)" ];
#    wantedBy = [ "graphical-session.target" ];
#    partOf = [ "graphical-session.target" ];
#    serviceConfig = {
#      ExecStart = "${pkgs.mako}/bin/mako";
#      RestartSec = 5;
#      Restart = "always";
#    };
#  };

  services.fprintd.enable = true;
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

